#!/bin/bash

if [[ ! -f db.sqlite3 ]]; then
    cp db_sharable.sqlite3 db.sqlite3
fi

yes | python3 manage.py makemigrations

python3 manage.py migrate

python3 manage.py runserver 0.0.0.0:8000
