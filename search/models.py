from django.core.paginator import Paginator
from django.db import models
from django.utils.translation import gettext_lazy as _
from django_countries import countries
from wagtail.admin.panels import FieldPanel
from wagtail.fields import StreamField
from wagtail.models import Page
from wagtail.search import index

from wdi_website.blocks import BodyBlock, CtaBlock
from wdi_website.models.models import Signature, NewsItem, Organization


class SearchResultsPage(Page):
    search_results = []
    search_query = ""
    subtitle = ""

    def get_context(self, request, **kwargs):
        context = super().get_context(request)

        context["search_results"] = self.search_results
        context["search_query"] = self.search_query

        return context
