from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.utils.translation import get_language_from_request
from django.utils.translation import gettext_lazy as _
from django.utils.translation import pgettext_lazy

from wagtail.core.models import Page
from wagtail.models import Locale
from wagtail.search.models import Query

from search.models import SearchResultsPage


def search(request):
    user_language = get_language_from_request(request, check_path=True)
    user_locale = Locale.objects.get(language_code=user_language)
    search_query = request.GET.get('query', None)
    page = request.GET.get('page', 1)

    # Search
    if search_query:
        search_results = Page.objects.filter(locale=user_locale).live().search(search_query)
        query = Query.get(search_query)

        # Record hit
        query.add_hit()
    else:
        search_results = Page.objects.none()

    # Rearrange search results to give prio to the ones with search term in title
    search_results = arrange_search_results(search_results, search_query)

    # Pagination
    paginator = Paginator(search_results, 15)
    try:
        search_results = paginator.page(page)
    except PageNotAnInteger:
        search_results = paginator.page(1)
    except EmptyPage:
        search_results = paginator.page(paginator.num_pages)

    results_page = SearchResultsPage(title=_("Search results"))
    results_page.search_results = search_results
    results_page.search_query = search_query
    results_page.subtitle = _("Pages that contain word(s)") + f" \"{search_query}\""
    return results_page.serve(request)


def arrange_search_results(search_results, query):
    """
    Moves all the pages that contain query in their title to the top of the list.
    """
    rearranged_results = []
    for result in search_results:
        if query in result.title:
            rearranged_results.insert(0, result)

        # We don't need to include a root page
        elif result.get_parent() is None:
            continue
        else:
            rearranged_results.append(result)
    return rearranged_results
