This manual will guide you through using an Ansible script to rebuild a WDI web app from scratch on a Virtual Private Server (a VPS).

## Prerequisites

 * Naturally, to run an Ansible script, you need to install Ansible. Check out this [documentation page](https://docs.ansible.com/ansible/latest/installation_guide/installation_distros.html) for details.
 * You need to have a recent website database ( `db.sqlite3` ) and `media` folder backup. 
 * You need access to DNS records management to be able to point DNS record for the website to the IP address of a new server. 
 
## Step 1 - obtain a VPS

To run a website, you need a server. In principle, any server that runs Linux and has a dedicated IP address should do. Here, we assume that a VPS is used, as it's the cheapest and easiest option.

Here are the things your server needs:

 * Dedicated IPv4 address. IPv6 is a plus, but not strictly necessary.
 * Linux distribution: this script has been tested on Ubuntu 22.04.
 * SSH access with private key authentication. [Here is a manual](https://www.digitalocean.com/community/tutorials/how-to-configure-ssh-key-based-authentication-on-a-linux-server) on setting it up.
 * At least 2GB RAM and at least 20GB storage space.

Options for VPS providers include DigitalOcean, Amazon Web Services, Microsoft Azure, Hostinger, RuVDS, etc.
DigitalOcean provides a quick and easy way to create droplets (VPS) with a private key for SSH already baked in.

Once the VPS is created, a Linux distribution is installed, and the private key auth is set up, note the IP addresses (IPv4 and IPv6, if available) of the VPS.

## Step 2 - set up DNS

Locate the service that currently manages the WDI domain names. Usually this will be a DDoS protection service or a DNS provider.

There, you will need to redirect the A (and AAAA, if your VPS has an IPv6 address) records for the WDI domain name(s) to the IP addresses of the VPS. 
Make sure to also redirect `www` records, so that, for instance, www.womensdeclaration.com can be resolved as well as https://womensdeclaration.com.


## Step 3 - fill in the variables of this Ansible script

Before we set up the WDI website on a new VPS, we need to fill in some variables in the inventory file.

Variables in `inventory.yaml`:
 * `hosts / < host IP >` (line 3): fill in the public IP address of your VPS. Mandatory.
 * `website_url` (line 6): fill in the domain name for the WDI website (without the www)
 * `dokku_users / username` (line 10): fill in the name of the user on your VPS that you will use for managing the website. Mandatory.
 * `dokku_users / ssh_key` (line 11): replace `~/.ssh/id_rsa.pub` with the path to the public key that you want to use to login to Dokku. This can be the same public key as you've set up to log in to your VPS.
 * `deepl_key`: API key to DeepL translation service. Not mandatory.
 * `smtp_host`, `smtp_user`, `smtp_password`: Credentials to SMTP server used for sending confirmation emails. Mandatory. 
 * `secret_key`: A [SECRET_KEY value used by Django](https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-SECRET_KEY). Set it to a long, hard to guess string. It doesn't have to persist between installations. Mandatory.
 * `brevo_api_key`: API key for Brevo service. Not mandatory, but highly recommended.
 * `to_address_org`: Email address of a user who will get a heads-up email when a new organisation has signed the Declaration. Mandatory.
 
 
Note that the values of variables in `inventory.yaml` are **sensitive**, and therefore this file in the repo contains example values. Don't share the real values with anyone. 


## Step 4 - install Ansible roles

Using your command line, navigate to the directory where `playbook.yaml`, `inventory.yaml`, and `requirements.yaml` are located.

```
ansible-galaxy install -r requirements.yaml
```

## Step 4 - run the playbook

Make sure your backups ( `db.sqlite3` and `media`) are copied to the same directory as where the `yaml` files are located.

Finally, run the playbook to re-deploy the website to your VPS:

```
ansible-playbook -i inventory.yaml playbook.yaml 
```

It can take a while, especially copying the `media` folder.


## Step 5 - check the results

Navigate to the domain name you have configured for the website in step 2. You should see the good old WDI website. If that's not the case, check the dokku logs, your VPS logs, or check out the section below.

## Additional considerations - TLS / SSL / HTTPS

HTTPS is the standard in modern web. To have it enabled, we need to obtain and periodically renew TLS certificates.

Currently, our TLS is managed by the DDoS protection provider. We don't really need to do anything special. If that situation has changed, we might need to do some extra work. Below are the few suggestions on where to start:

 * See if the current DDoS protection / DNS provider allows to quickly enable TLS for the managed domain names.
 * Check out [dokku-letsencrypt](https://github.com/dokku/dokku-letsencrypt) plugin for Dokku, which enables hassle-free installation of [Letsencrypt](https://letsencrypt.org/) TLS certificats.

