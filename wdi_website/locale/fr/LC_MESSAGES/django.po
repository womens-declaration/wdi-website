# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-07-28 21:29+0200\n"
"PO-Revision-Date: 2023-12-30 21:09+0100\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 3.4.1\n"

#: blocks.py:53
msgid "Sign the Declaration form: no config"
msgstr "Formulaire de signature"

#: blocks.py:76
msgid "Stats about the Declaration: no config"
msgstr "Statistiques sur la déclaration"

#: blocks.py:103
msgid "Supporting Orgs List: no config"
msgstr "Organismes d’appui"

#: blocks.py:181
msgid "Female symbol page divider: no config"
msgstr "Female symbol page divider: no config"

#: blocks.py:193
msgid ""
"A simple paragraph with a title and rich text. For youtube.com embeds, "
"ending the url with \"&rel=0\" will ensure only videos from the same channel "
"will be advertised at the end."
msgstr ""

#: blocks.py:204
msgid "A paragraph with a title, rich text, and image"
msgstr "A paragraph with a title, rich text, and image"

#: blocks.py:211
msgid "Country Contact Form: no config"
msgstr "Formulaire de demande pour coordinatrice nationale"

#: blocks.py:216
msgid "Volunteer Form: no config"
msgstr "Formulaire de bénévolat"

#: blocks.py:221
msgid "Donate Widget: no config"
msgstr "Widget pour contributions"

#: blocks.py:239
msgid "Social media icons with external links."
msgstr "Icônes de réseaux sociaux avec liens externes."

#: blocks.py:252
msgid ""
"Embed an external resource or website. For an embedded google form link, get "
"the url from the src=\"\" portion of the embed option under \"Send\" in "
"google forms. The button name (e.g. \"XX form\" or \"Watch Video\") and "
"source of resource (e.g. \"(via Google)\" or \"(via Youtube)\") are for the "
"no-cookie option."
msgstr ""
"Incorporer une ressource ou site web externe. D’un formulaire Google,copier "
"le URL du src=\"\" portion à incorporer sous \"Send\" dans lesformulaires "
"Google."

#: forms.py:19
msgid "Enter the letters from the image. Click the image if unreadable"
msgstr "Écrire les lettres de l’image. Cliquez l’image si elle est illisible."

#: forms.py:58
msgid "International?"
msgstr "International?"

#: forms.py:59
msgid "Is this organization international? (will override country)"
msgstr "Cette organisation est-elle internationale ? (prévaut sur le pays)"

#: forms.py:63 models/models.py:139
msgid "Organization type"
msgstr "Type d'organisation"

#: forms.py:64
msgid "What does this organization do?"
msgstr "Que fait cette organisation ?"

#: forms.py:69
msgid "Organization logo"
msgstr "Logo de l'organisation"

#: forms.py:70
msgid "Upload the logo for your organization here."
msgstr "Téléchargez le logo de votre organisation ici."

#: forms.py:74
msgid "Organization homepage"
msgstr "Page d'accueil de l'organisation"

#: forms.py:75
msgid "URL for website or social media"
msgstr "URL du site web ou des médias sociaux"

#: forms.py:79
msgid "Can we mention support?"
msgstr "Pouvons-nous mentionner votre soutien ?"

#: forms.py:81
msgid ""
"The organization gives permission to WDI to mention to policy makers, "
"journalists and politicians that this organization has signed the "
"Declaration."
msgstr ""
"L'organisation donne la permission à WDI de mentionner aux décideurs, aux "
"journalistes et aux politiciens que cette organisation a signé la "
"Déclaration."

#: forms.py:87
msgid "Can we use the logo?"
msgstr "Pouvons-nous utiliser votre logo ?"

#: forms.py:89
msgid ""
"The organization gives permission for WDI to replicate its logo in publicity "
"about the Declaration, and materials promoting the Declaration."
msgstr ""
"L'organisation donne la permission à la WDI de reproduire son logo dans la "
"publicité sur la Déclaration et dans les documents de promotion de la "
"Déclaration."

#: forms.py:109
msgid ""
"If you are signing for your organization, we need your organization's "
"homepage."
msgstr ""
"Si vous signez pour votre organisation, nous avons besoin de la page "
"d'accueil de votre organisation."

#: forms.py:118
msgid ""
"If you are signing for your organization, we need your organization's name."
msgstr ""
"Si vous signez pour votre organisation, nous avons besoin du nom de votre "
"organisation."

#: forms.py:127
msgid ""
"If you are signing for your organization, we need your organization's type."
msgstr ""
"Si vous signez pour votre organisation, nous avons besoin du type de votre "
"organisation."

#: forms.py:136
msgid "The organization logo is in wrong format."
msgstr "Le logo de l'organisation n'est pas dans le bon format."

#: forms.py:144
msgid "The organization logo is too large."
msgstr "Le logo de l'organisation est trop grand."

#: models/models.py:28
msgid "Your first and last name"
msgstr "Prénom et nom de famille"

#: models/models.py:29
msgctxt "Signatory name"
msgid "Name"
msgstr "Nom "

#: models/models.py:32
msgid "Your email"
msgstr "Quel est votre courriel ?"

#: models/models.py:32
msgid "Email"
msgstr "Courriel "

#: models/models.py:35
msgid "Get email updates?"
msgstr "Recevoir des bulletins d'information par courrier."

#: models/models.py:37
msgid "Subscribe to emails?"
msgstr "Vous abonnez aux courriels ?"

#: models/models.py:41
msgid "Your affiliated group or company (optional)"
msgstr "Indiquez votre affiliation (optionnel)"

#: models/models.py:43 views/submit_signature.py:62
msgid "Organization"
msgstr "Organisme"

#: models/models.py:46
msgid "Sign on behalf of this organization?"
msgstr "Signer au nom de l’organisation ?"

#: models/models.py:48
msgid "Sign for organization?"
msgstr "Signez au nom de l’organisme ?"

#: models/models.py:51
msgid "What is your country?"
msgstr "Quel est votre pays?"

#: models/models.py:51 models/models.py:144
msgid "Country"
msgstr "Pays "

#: models/models.py:60
msgid "Your state or province (optional)"
msgstr "Votre état ou province  (optionnel)"

#: models/models.py:61
msgid "State/Province"
msgstr "État/Province"

#: models/models.py:65
msgid "Checked if this signatory confirmed their email."
msgstr "Vérification que le courriel de la signataire est confirmé"

#: models/models.py:68
msgid "Email confirmed"
msgstr "Courriel confirmé"

#: models/models.py:73
msgid "Which language of the website did this user use to sign?"
msgstr ""
"Quelle langue du site web cet utilisateur a-t-il utilisée pour signer ?"

#: models/models.py:76
msgid "Language"
msgstr "Langue"

#: models/models.py:80
msgid ""
"Indicates if this email has been automatically synchronized to the mail "
"sending system. Do not change this value."
msgstr ""

#: models/models.py:123
msgid "Activist group"
msgstr "Groupe d'activistes"

#: models/models.py:124
msgid "Non-activist nonprofit"
msgstr "Association non militante à but non lucratif"

#: models/models.py:125
msgctxt "A noun"
msgid "Business"
msgstr "Entreprise"

#: models/models.py:126
msgid "Website, blog, online resource"
msgstr "Site web, blog, ressource en ligne"

#: models/models.py:127
msgctxt "As in, other kind of Organization"
msgid "Other"
msgstr "Autre"

#: models/models.py:132
msgid "Name of the organization"
msgstr "Nom de l'organisation"

#: models/models.py:133
msgid "Organization name"
msgstr "Nom de l'organisation"

#: models/models.py:138
msgid "Type of the organization: activist group, business, ..."
msgstr "Type d'organisation : groupe d'activistes, entreprise, ..."

#: models/models.py:144
msgid "Where is this org based?"
msgstr "Où est basée l’organisation ?"

#: models/models.py:147 templates/blocks/org_logo_link.html:17
#: templates/blocks/org_logo_link.html:24
msgid "International"
msgstr "International"

#: models/models.py:148
msgid "Is this organization international?"
msgstr "Cette organisation est-elle internationale ?"

#: models/models.py:152
msgid "A square or circle logo"
msgstr "Logo circulaire ou carré"

#: models/models.py:153
msgid "Logo"
msgstr "Logo"

#: models/models.py:158
msgid "Link to the organization's homepage"
msgstr "Lien à la page d’accueil de l’organisation"

#: models/models.py:160
msgid "Homepage"
msgstr "Page d’accueil"

#: models/models.py:165
msgid "Contact email of this organization"
msgstr "Signer au nom de l’organisation ?"

#: models/models.py:166
msgid "Contact Email"
msgstr "Courriel de contact"

#: models/models.py:170
msgid ""
"If the organization gives permission to WDI to mention to policy makers, "
"journalists and politicians that this organisation has signed the "
"Declaration."
msgstr ""
"Si l'organisation donne la permission à la WDI de mentionner aux décideurs, "
"aux journalistes et aux politiciens que cette organisation a signé la "
"Déclaration."

#: models/models.py:174
msgid "Permission to mention Support"
msgstr "Autorisation de mentionner le soutien"

#: models/models.py:178
msgid ""
"If the organization gives permission for WDI to replicate its logo in "
"publicity about the Declaration, and materials promoting the Declaration."
msgstr ""
"Si l'organisation donne la permission à la WDI de reproduire son logo dans "
"la publicité sur la Déclaration, et dans le matériel de promotion de la "
"Déclaration."

#: models/models.py:182
msgid "Permission to use the Logo"
msgstr "Autorisation d'utiliser le logo"

#: models/models.py:186
msgid ""
"Checked if this organization's data is checked and confirmed, and the "
"organization should be displayed in the Organizations block"
msgstr ""

#: models/models.py:190
msgid "Organization data confirmed"
msgstr "Nom de l'organisation"

#: models/models.py:200
msgid "The headline of the article"
msgstr "L’entête de l’article"

#: models/models.py:201
msgid "Headline"
msgstr "Entête"

#: models/models.py:205
msgid "A few lines of preview text for the article"
msgstr "Aperçu de quelques lignes de l’article"

#: models/models.py:206
msgid "Preview text"
msgstr "Aperçu du texte"

#: models/models.py:209
msgid "A square image to represent the post"
msgstr "Une image carrée pour représenter le post"

#: models/models.py:209
msgid "Image"
msgstr "Image"

#: models/models.py:213
msgid "Link to the original source"
msgstr "Lien à la source d’origine"

#: models/models.py:215
msgid "Article link"
msgstr "Lien à l'article"

#: models/models.py:219
msgid "Call to action button link"
msgstr "Bouton : Appel à l’action"

#: models/models.py:222
msgid "CTA link"
msgstr "CTA lien"

#: models/models.py:226 models/models.py:233
msgid "Prompt the user to take action"
msgstr "Prompt the user to take action"

#: models/models.py:229
msgid "CTA title"
msgstr "CTA titre"

#: models/models.py:236
msgid "CTA subtitle"
msgstr "CTA sous-titre"

#: models/models.py:239
msgid "A square or round logo or image representing the call to action"
msgstr "CTA Logo ou image circulaire ou carré"

#: models/models.py:242
msgid "CTA image"
msgstr "CTA image"

#: models/models.py:247
msgid "Which language will the user read news in?"
msgstr "Dans quelle langue l'utilisateur lira-t-il les nouvelles ?"

#: models/models.py:250
msgid "News Language"
msgstr "Langue"

#: models/models.py:259
msgid "Your Facebook page URL"
msgstr "URL de votre page Facebook"

#: models/models.py:260
msgid "Your Twitter profile URL"
msgstr "URL de votre profil Twitter"

#: models/models.py:261
msgid "Your YouTube channel or user account URL"
msgstr "URL de votre chaîne YouTube ou URL du compte usager"

#: models/models.py:262
msgid "Your Spotify podcast or user account URL"
msgstr "URL de votre chaîne Spotify ou URL du compte usager"

#: models/pages.py:17 models/pages.py:39 models/pages.py:81 models/pages.py:114
msgid "A subtitle to show on the header of the page"
msgstr "Sous-titre pour les entêtes de pages"

#: models/pages.py:19 models/pages.py:41 models/pages.py:83
msgid "Subtitle"
msgstr "Sous-titre"

#: models/pages.py:22 models/pages.py:44 models/pages.py:86 models/pages.py:119
msgid "Header CTAs"
msgstr "Entêtes CTA"

#: models/pages.py:23
msgid "Body"
msgstr "Corps"

#: models/pages.py:116
msgid "Subtitles"
msgstr "Sous-titres"

#: templates/base.html:93
msgid "Cookie settings"
msgstr "Paramètres des témoins"

#: templates/base.html:107
msgid ""
"This website uses cookies to ensure you get the best experience on our "
"website."
msgstr "Ce site utilise des témoins pour en améliorer l&#39;efficacité."

#: templates/base.html:108
msgid "Cookie policy"
msgstr "Politique relative aux témoins"

#: templates/base.html:109
msgid "Allow cookies"
msgstr "Accepter les témoins"

#: templates/base.html:110
msgid "Decline"
msgstr "Refuser"

#: templates/base.html:159
msgctxt "The placeholder in a global search bar"
msgid "Search"
msgstr "Rechercher"

#: templates/base.html:222
msgid "About WDI"
msgstr "À propos du WDI"

#: templates/base.html:223 templates/wdi_website/confirmation_email.html:509
#: views/submit_signature.py:51 views/submit_signature.py:79
msgid ""
"Women's Declaration International is a group of volunteer women who are "
"dedicated to preserving our sex-based rights. Join us in defending women and "
"girls."
msgstr ""
"Le groupe WDI est un groupe de femmes bénévoles dédiées à la préservation de "
"nos droits liés au sexe biologique. Joignez-vous à notre combat pour la "
"défence des femmes et des filles."

#: templates/base.html:231 templates/wdi_website/confirmation_email.html:528
#: templates/wdi_website/org_signup_email.html:526
msgid "Mailing address:"
msgstr "Adresse postale :"

#: templates/base.html:239
msgid "Site Map"
msgstr "Plan du site"

#: templates/base.html:250
msgid "Social"
msgstr "Social"

#: templates/base.html:265
msgid "Contribute on Gitlab"
msgstr "Impliquez-vous sur Gitlab"

#: templates/base.html:272
msgid "The"
msgstr "Le"

#: templates/base.html:272
msgid "code of this website"
msgstr "code de ce site"

#: templates/base.html:272
msgid "is published under the AGPL3, and the content under CC-BY-NC-ND."
msgstr "est publié sous AGPL3, et le contenu sous CC-BY-NC-ND."

#: templates/blocks/country_contact_form.html:29
#: templates/blocks/donate_widget.html:30
#: templates/blocks/embed_resource_block.html:30
#: templates/blocks/volunteer_form.html:31
msgid "Loading..."
msgstr "Chargement..."

#: templates/blocks/country_contact_form.html:42
msgid "Country Contact Form"
msgstr "Formulaire de demande pour coordinatrice nationale"

#: templates/blocks/country_contact_form.html:42
#: templates/blocks/volunteer_form.html:44
msgid "(via Google)"
msgstr ""

#: templates/blocks/declaration_stats.html:5
msgid "Individual Signatures"
msgstr "Signatures d’individus"

#: templates/blocks/declaration_stats.html:7
msgid "from"
msgstr "dans"

#: templates/blocks/declaration_stats.html:9
msgid "Countries"
msgstr "Pays"

#: templates/blocks/declaration_stats.html:11
msgid "in collaboration with"
msgstr "en collaboration avec"

#: templates/blocks/declaration_stats.html:13
msgctxt "in Declaration stats"
msgid "Organizations"
msgstr "Organismes"

#: templates/blocks/donate_widget.html:43
msgid "Donate"
msgstr "Faire un don"

#: templates/blocks/featured_organizations.html:3
msgid "Featured Organizations"
msgstr "Organismes en vedette"

#: templates/blocks/featured_organizations.html:13
msgid "View All Supporting Organizations"
msgstr "Voir toutes les organismes d'appui"

#: templates/blocks/featured_signatories_list_block.html:7
#, fuzzy
#| msgid "Featured Organizations"
msgid "Featured Signatories"
msgstr "Signataires en vedette"

#: templates/blocks/org_list.html:7
msgid "Supporting Organizations"
msgstr "Organismes d'appui"

#: templates/blocks/org_list.html:16
msgid "Activist groups"
msgstr "Groupes d'activistes"

#: templates/blocks/org_list.html:25
msgid "Businesses"
msgstr "Entreprises"

#: templates/blocks/org_list.html:35
msgid "Nonprofit organizations"
msgstr "Organisations à but non lucratif"

#: templates/blocks/org_list.html:45
msgid "Online resources"
msgstr "Ressources en ligne"

#: templates/blocks/sign_dec_form.html:22
msgid "Sign the Declaration"
msgstr "Signez la déclaration"

#: templates/blocks/sign_dec_form.html:24
msgid "We will send you an email to confirm your email address."
msgstr "Nous vous enverrons un courriel confirmant votre information."

#: templates/blocks/sign_dec_form.html:26
msgid "(If you do not see the email, try checking your spam folder.)"
msgstr ""
"(Si vous ne voyez pas le courriel, jetez d'abord un coup d'oeil à votre "
"dossier spam.)"

#: templates/blocks/sign_dec_form.html:28
msgid "After the confirmation, your name will be added to the public list of "
msgstr "Après la confirmation, votre nom apparaîtra sur la liste publique des "

#: templates/blocks/sign_dec_form.html:29
msgid "signatories"
msgstr "signataires"

#: templates/blocks/sign_dec_form.html:30
msgid "By signing, you agree to our"
msgstr "En signant, vous accepter notre"

#: templates/blocks/sign_dec_form.html:30
#: templates/blocks/sign_dec_form.html:44
msgid "Privacy Policy"
msgstr "Politique de confidentialité"

#: templates/blocks/sign_dec_form.html:38
msgid "Submit"
msgstr "Soumettre"

#: templates/blocks/sign_dec_form.html:42
msgid "Privacy policy confirmation:"
msgstr "Politique de confidentialité:"

#: templates/blocks/sign_dec_form.html:44
msgid "I have read and accept"
msgstr "J'ai lu et j'accepte"

#: templates/blocks/volunteer_form.html:44
msgid "Volunteer Form"
msgstr "Formulaire de bénévolat"

#: templates/error.html:21
msgid "Return to the main page"
msgstr "Retour à la page d’accueil"

#: templates/modeladmin/wdi_website/organization/index.html:12
msgid "Download organization data as CSV"
msgstr "Télécharger les infos de l’organisation en format CSV"

#: templates/modeladmin/wdi_website/organization/index.html:13
#: templates/modeladmin/wdi_website/signature/index.html:14
msgid "Download data"
msgstr "Télécharger les infos"

#: templates/modeladmin/wdi_website/signature/index.html:13
msgid "Download signature data as CSV"
msgstr "Télécharger l’info de la signature en format CSV"

#: templates/wdi_website/confirmation_email.html:218
#: views/submit_signature.py:27
msgid "Thank you for signing the Declaration!"
msgstr "Merci d’avoir signé la déclaration !"

#: templates/wdi_website/confirmation_email.html:235
msgid ""
"Please click the button below to confirm your signature and let it appear on "
"the public Signatories list:"
msgstr ""
"SVP cliquer le bouton ci-dessous afin de confirmer votre signature et "
"permettre sa publication sur la liste des signataires."

#: templates/wdi_website/confirmation_email.html:274
msgid "Confirm my signature"
msgstr "Confirmer ma signature"

#: templates/wdi_website/confirmation_email.html:293
msgid "Or copy and paste the following link to your browser:"
msgstr "Ou copier et coller ce lien dans votre fureteur :"

#: templates/wdi_website/confirmation_email.html:377
#: views/submit_signature.py:38
msgid ""
"We would like to thank you for helping to protect sex-based rights of women "
"and girls. The change starts with women and allies just like you. Your "
"support means the world to us. Thank you!"
msgstr ""
"Nous vous remercions de votre aide pour la protection des droits des femmes "
"et des filles fondés sur le sexe. Le changement s’initie avec des femmes et "
"alliées comme vous. Votre soutien nous est essentiel. Merci !"

#: templates/wdi_website/confirmation_email.html:430
#: views/submit_signature.py:43
msgid ""
"If the button above doesn't work, please let us know by replying to this e-"
"mail."
msgstr ""
"Si le bouton ci-dessus ne fonctionne pas, svp nous en aviser en répondant à "
"ce courriel."

#: templates/wdi_website/confirmation_email.html:436
#: views/submit_signature.py:47
msgid ""
"If you did not sign the Declaration on Women's Sex-Based rights, please "
"disregard this message."
msgstr "Si vous n’avez pas signé la déclaration, svp ignorer ce message."

#: templates/wdi_website/country_list_page.html:15
msgid "Citizens of"
msgstr "Des citoyennes de"

#: templates/wdi_website/country_list_page.html:15
msgid "countries have signed the Declaration:"
msgstr "pays ont signé la déclaration :"

#: templates/wdi_website/country_list_page.html:55
msgctxt "As in, the number of signatures"
msgid "signatures"
msgstr "signataires"

#: templates/wdi_website/event_list_page.html:10
msgid "Upcoming Events"
msgstr "Événements à venir"

#: templates/wdi_website/event_list_page.html:26
msgid "Sign up!"
msgstr "Enregistrez-vous !"

#: templates/wdi_website/event_list_page.html:32
msgid "Sorry, no upcoming events right now."
msgstr "Désolées, aucun événement au calendrier en ce moment."

#: templates/wdi_website/event_list_page.html:34
msgid "Get involved"
msgstr "Impliquez-vous"

#: templates/wdi_website/event_list_page.html:34
msgid "instead, and help us plan events!"
msgstr "et aidez-nous à planifier les événements!"

#: templates/wdi_website/event_list_page.html:36
msgid "Or, support us by donating:"
msgstr "Ou soutenez-nous en faisant une contribution :"

#: templates/wdi_website/news_item_list_page.html:10
msgid "Women's Rights News"
msgstr "Nouvelles sur les droits des femmes"

#: templates/wdi_website/news_item_list_page.html:12
msgid "Share your news with us at:"
msgstr "Partager vos nouvelles avec nous :"

#: templates/wdi_website/partials/captcha.html:5
msgid "If you cannot read this image, please contact us at"
msgstr "Si cette page est illisible, écrivez-nous à :"

#: views/errors.py:10
msgid "Sorry, this page does not exist."
msgstr "Désolées, cette page n’existe pas"

#: views/errors.py:20
msgid "Sorry. Something went wrong on our side. Try again later."
msgstr "Désolées. Une erreur s’est produite de notre côté. Ressayer plus tard."

#: views/errors.py:31
msgid "Sorry. You do not have a permission to access this resource."
msgstr "Désolées. Vous n’avez pas accès à cette ressource."

#: views/errors.py:42
msgid ""
"Sorry. There is something wrong with data you have sent. Check your input "
"and try again."
msgstr ""
"Désolées. Une erreur s'est glissée dans votre formulaire. SVP vérifier que "
"toutes les infos sont correctes, et essayer de nouveau"

#: views/submit_signature.py:30
msgid ""
"Please use this link to confirm your signature and let it appear on the "
"public Signatories list:"
msgstr ""
"Utiliser ce lien pour confirmer votre signature et permettre sa publication "
"sur la liste des signatures."

#: views/submit_signature.py:66
msgid "has signed up the declaration."
msgstr ""

#: views/submit_signature.py:69
msgid ""
"Please keep in mind that anyone might try to sign the Declaration in the "
"name of a certain organization. Each signature should be confirmed by the "
"said organization and carefully reviewed by the WDI team before being "
"accepted as genuine. For this same reason, proceed with caution when "
"following any links provided on a signature."
msgstr ""

#: views/submit_signature.py:75
msgid ""
"If you have any concerns that an organization signature might be false, "
"please contact info@womensdeclaration.com."
msgstr ""

#: views/submit_signature.py:112
msgid "Thank you for signing the Declaration! Please confirm your signature."
msgstr ""
"Merci d’avoir signé la déclaration ! Veuillez confirmer votre signature. "

#: views/submit_signature.py:156
#, python-brace-format
msgid "Signature associated with email {email} not found."
msgstr "Signature associée à l'email {email} non trouvée."

#: views/submit_signature.py:167
#, python-brace-format
msgid "Signature by {name} ({email}) has already been confirmed."
msgstr ""

#: views/submit_signature.py:182
#, python-brace-format
msgid "Confirmation email to {name} ({email}) sent successfully."
msgstr ""

#: views/submit_signature.py:191
#, python-brace-format
msgid "Failed to send the confirmation email to {name} ({email})."
msgstr ""

#: views/submit_signature.py:213
msgid "New organization sign-up: {}"
msgstr ""

#: views/submit_signature.py:270 views/submit_signature.py:341
msgid "There was an error processing your form. Please try again."
msgstr "Une erreur de transmission s’est produite. SVP ressayer."

#: views/submit_signature.py:277
msgid ""
"Thank you for signing the Declaration! Please check your inbox and confirm "
"your signature!"
msgstr ""
"Merci d’avoir signé la déclaration ! Vérifier vos boîtes de réception pour "
"le courriel de confirmation."

#: views/submit_signature.py:292
msgid "You have already signed the Declaration!"
msgstr "Vous avez déjà signé cette déclaration !"

#: views/submit_signature.py:306
msgid "Incorrect Captcha solution. Please try again."
msgstr "Mauvaise réponse Cacha. Ressayer."

#: views/submit_signature.py:322
msgid ""
"There is something wrong with your form. Please check that all the values "
"are correct and try again."
msgstr ""
"Il y a une erreur dans le formulaire. SVP vérifier que toutes les infos sont "
"correctes et essayer de nouveau."

#: views/submit_signature.py:372
msgid ""
"The confirmation code you've tried to use is invalid. Please use the "
"confirmation code you've received in an e-mail."
msgstr ""
"Le code de confirmation que vous avez entré est erroné. SVP vérifier le code "
"contenu dans votre courriel."

#: views/submit_signature.py:387
msgid "You have already confirmed your signature before."
msgstr "Vous aviez déjà confirmé votre signature."

#: views/submit_signature.py:408
msgid ""
"Thank you! You have confirmed your signature. It will now be visible on the "
"signatories page."
msgstr ""
"Merci ! Votre signature est confirmée. Elle apparaîtra sur la page des "
"signataires. "

#: wagtail_hooks.py:51
msgid "Signatures"
msgstr "Signataires"

#: wagtail_hooks.py:72
msgid "Organizations"
msgstr "Organismes"

#: wagtail_hooks.py:99
msgid "News Item"
msgstr "Nouvelle"

#~ msgid "A cta with external link."
#~ msgstr "Cta avec lien externe."

#~ msgid "The title of the event"
#~ msgstr "Titre de l'événement"

#~ msgid "Title"
#~ msgstr "Titre"

#~ msgid "The description of the event"
#~ msgstr "Description de l’événement"

#~ msgid "Description"
#~ msgstr "Description"

#~ msgid "The location of the event"
#~ msgstr "Lieu de l’événement"

#~ msgid "Location"
#~ msgstr "Lieu"

#~ msgid "The start time of the event"
#~ msgstr "Heure de début de l’événement"

#~ msgid "Start time"
#~ msgstr "Temps au début"

#~ msgid "End time"
#~ msgstr "Temps à la fin"

#~ msgid "An image that represents the event"
#~ msgstr "Image représentant l’événement"

#~ msgid "Link to the event signup page"
#~ msgstr "Lien pour s’enregistrer à l’événement"

#~ msgid "Event link"
#~ msgstr "Lien à l’événement"

#~ msgid "Event"
#~ msgstr "Événement"

#~ msgid "The organizations will be exported to a spreadsheet file (*.csv)"
#~ msgstr "Les organisations seront exportées ver un fichier *.csv"

#~ msgid "The signatures will be exported to a spreadsheet file (*.csv)"
#~ msgstr "Les signatures seront exportées en un fichier *.csv"

#, fuzzy
#~| msgid "Name of the organization"
#~ msgid "Non-profit organization"
#~ msgstr "Nom de l'organisation"

#~ msgid "Is this org international? (will override country)"
#~ msgstr "L’organisation est-elle internationale ?"

#, fuzzy
#~| msgid "Sign for organization?"
#~ msgid "As in, other kind of Organization"
#~ msgstr "Signez au nom de l’organisme ?"

#~ msgid ""
#~ "This confirmation code is too old. Check your e-mail inbox for a new "
#~ "confirmation code."
#~ msgstr ""
#~ "Le code de confirmation est expiré. Vérifier vos courriels pour un "
#~ "nouveau code de confirmation."

#~ msgid "A simple paragraph with a title and rich text"
#~ msgstr "A simple paragraph with a title and rich text"
