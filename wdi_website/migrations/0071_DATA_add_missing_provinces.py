# Generated by Django 3.0.14 on 2021-06-27 13:12
from difflib import SequenceMatcher

import unidecode
from django_countries import countries
from django.db import migrations
from iso3166_2 import ISO3166_2

import re

# Mapping some old names of popular provinces names to their ISO code.
# These provinces will be updated with the right code and the 2nd name in the tuple.
popular_provinces_mapping = {
    # code: (old name, name that will be used)
    'KR-11': ("Soul-t'ukpyolsi", "Seoul"),
    'KR-41': ("Kyonggi-do", "Gyeonggi"),
    'MX-CMX': ("Distrito Federal", "Mexico City"),
    'FR-IDF': ("Ile-de-France", "Ile-de-France"),
    'CL-RM': ("Region Metropolitana (Santiago)", "Región Metropolitana de Santiago"),
    'KR-28': ("Inch'on-gwangyoksi", "Incheon"),
    'DE-NW': ("Nordrhein-Westfalen", "Nordrhein-Westfalen"),
    'FR-ARA': ("Rhone-Alpes", "Auvergne-Rhône-Alpes"),
    'ES-BI': ("Biscay", "Bizkaia"),
    'ES-A': ("Alicante", "Alacant"),
    'CO-DC': ("Distrito Capital de Santa Fe de Bogota", "Distrito Capital de Bogotá"),
    'KR-26': ("Pusan-gwangyoksi", "Busan-gwangyeoksi"),
    'ES-SE': ("Seville", "Sevilla"),
    'KR-27': ("Taegu-gwangyoksi", "Daegu-gwangyeoksi"),
    'KR-48': ("Kyongsang-namdo", "Gyeongsangnam-do"),
    'KR-44': ("Ch'ungch'ong-namdo", "Chungcheongnam-do"),
    'BE-BRU': ("Brussels Capitol Region", "Brussels Hoofdstedelijk Gewest"),
}


def replace_province_hardcoded(country_code, province_code, Province):
    """
    Update the well-known province using the `popular_provinces_mapping` dictionary above
    Updates the name of the province and adds an ISO3166-2 code
    """
    old_province_name = popular_provinces_mapping[province_code][0]
    new_province_name = popular_provinces_mapping[province_code][1]

    old_province = Province.objects.filter(country=country_code, name=old_province_name).get()

    old_province.name = new_province_name
    old_province.iso3166_2_code = province_code
    old_province.save()


def find_new_province_existing_twin(existing_provinces, country_code, province_code, iso):
    """
    Finds an existing province with almost the same name as the new province's normalised name.
    Updates the existing province with the correct ISO3166-2 code.
    """
    new_province = iso[country_code][province_code]
    new_province_name = get_province_name(new_province)
    if not new_province_name:
        return False

    for existing_province in existing_provinces:
        if province_names_almost_match(existing_province.name, new_province_name):
            # A very similar province name found - update the ISO code and leave it be
            existing_province.iso3166_2_code = province_code
            existing_province.save()
            return True


def get_province_name(province_iso3166_2):
    """
    Some provinces include extra information in their name - like province code or alternative spelling.
    This function removes these extra notes, leaving only "canonical" name.
    """
    # Some Spanish subdivisions include stars in their names. They are pretty, but we don't want them.
    name = province_iso3166_2["name"].replace("*", "")

    # Checks for brackets and removes them if found
    if "[" not in name:
        return name

    # Describes a string that may or may not have a square brackets part.
    # If it does, real name is the part before brackets.
    # Example: Uppsala län [SE-03]
    name_without_square_brackets_regex = re.compile(r"(?P<name>[\w -']+\w)( \[.*\])?")
    match = name_without_square_brackets_regex.match(name)
    if not match:
        return None

    return match.group("name")


def province_names_almost_match(name1, name2):
    name1_slugified = unidecode.unidecode(name1).lower()
    name2_slugified = unidecode.unidecode(name2).lower()

    similarity_ratio = SequenceMatcher(a=name1_slugified, b=name2_slugified).ratio()
    return similarity_ratio >= 0.95


def populate_provinces(apps, schema_editor):
    Province = apps.get_model("wdi_website", "Province")
    iso = ISO3166_2()
    country_names = list(countries)

    for country_code, country_name in country_names:
        # NZ has been taken care of, GB will be in the next migration
        if country_code in ["NZ", "GB"]:
            continue

        new_provinces = iso[country_code]
        existing_provinces = Province.objects.filter(country=country_code)

        for new_province_code in new_provinces:

            # Check if we have hardcoded the correct entry for this province.
            # If we have, update the name and ISO code of the existing object
            if new_province_code in popular_provinces_mapping:
                replace_province_hardcoded(country_code, new_province_code, Province)
                continue

            # No hardcoded option - try to find a similar province among the existing ones and update its code
            else:
                twin_found = find_new_province_existing_twin(existing_provinces, country_code, new_province_code, iso)

            # This subdivision did not exist in the old set - create it
            if not twin_found:
                new_province = iso[country_code][new_province_code]
                p = Province(name=get_province_name(new_province), country=country_code,
                             iso3166_2_code=new_province_code)
                p.save()


def delete_all_unclaimed_provinces(apps, schema_editor):
    Province = apps.get_model("wdi_website", "Province")

    # This will make sure that old provinces won't show up in form, while preserving the foreign key on Signature model
    # Only "unclaimed" subdivisions that have a default ISO code are affected
    # (except GB, which will be taken care of in the next migration)
    Province.objects.filter(iso3166_2_code="-").exclude(country="GB").update(country="OUTDATED")


class Migration(migrations.Migration):
    dependencies = [
        ("wdi_website", "0070_DATA_add_NZ_provinces"),
    ]

    operations = [migrations.RunPython(populate_provinces), migrations.RunPython(delete_all_unclaimed_provinces)]
