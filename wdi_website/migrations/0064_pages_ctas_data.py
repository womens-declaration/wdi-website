from django.db import migrations
from wagtail.blocks import StreamValue


import logging


def get_console_logger():
    logger = logging.getLogger(__name__)
    handler = logging.StreamHandler()
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)

    logger.info("starting logger")
    return logger


def update_all_pages_with_ctas(apps, schema_editor):
    for page_class in ["CountryListPage", "SignatureListPage", "NewsItemListPage"]:
        update_page_class_header_ctas(apps, page_class)

    update_standard_page_ctas(apps)


def update_standard_page_ctas(apps):
    """
    Special case for StandardPage, which has CTAs in header and body.
    """
    PageClass = apps.get_model("wdi_website", "StandardPage")

    for p in PageClass.objects.all():
        update_page_header_ctas(p)
        update_page_body_ctas(p)


def update_page_class_header_ctas(apps, page_class_name):
    """
    For every page of a given class, updates all the CTAs in its header to a new structure.
    """
    PageClass = apps.get_model("wdi_website", page_class_name)

    for p in PageClass.objects.all():
        update_page_header_ctas(p)


def update_page_body_ctas(page):
    stream_data, stream_data_updated = update_cta_stream_data(page.body.raw_data)

    if stream_data_updated:
        stream_block = page.body.stream_block
        page.body = StreamValue(stream_block, stream_data, is_lazy=True)
        page.save()


def update_page_header_ctas(page):
    stream_data, stream_data_updated = update_cta_stream_data(page.header_ctas.raw_data)

    if stream_data_updated:
        stream_block = page.header_ctas.stream_block
        page.header_ctas = StreamValue(stream_block, stream_data, is_lazy=True)
        page.save()


def update_cta_stream_data(stream_data):
    new_stream_data = []
    updated = False

    for block in stream_data:
        if block['type'] == 'cta_block':  # Body CTA block, has external URLs in it
            button_link = block["value"]["button_link"]
            if type(button_link) is str:  # Double check to make sure we don't corrupt a new-style content
                block["value"]["button_link"] = {"page": None, "external_url": button_link}
                updated = True

        elif block['type'] == 'cta':  # Header CTA, has a page link in it
            button_link = block["value"]["button_link"]
            if type(button_link) is int:  # Double check to make sure we don't corrupt a new-style content
                block["value"]["button_link"] = {"page": button_link, "external_url": ""}
                updated = True

        new_stream_data.append(block)

    return new_stream_data, updated


def forward(apps, schema_editor):
    logger = get_console_logger()

    StandardPage = apps.get_model("wdi_website", "StandardPage")

    for p in StandardPage.objects.all():
        stream_data = []
        mapped = False

        for block in p.body.raw_data:
            if block['type'] == 'cta_block':
                button_link = block["value"]["button_link"]
                logger.info(button_link)
                if type(button_link) is str:  # Double check to make sure we don't corrupt a new-style content
                    logger.info('button_link is str')
                    block["value"]["button_link"] = {"page": None, "external_url": button_link}
                    logger.info(block)
                    mapped = True

                stream_data.append(block)

            else:
                stream_data.append(block)

        if mapped:
            stream_block = p.body.stream_block
            p.body = StreamValue(stream_block, stream_data, is_lazy=True)
            p.save()


class Migration(migrations.Migration):
    dependencies = [
        ("wdi_website", "0064_alter_countrylistpage_header_ctas_and_more"),
    ]

    operations = [
        migrations.RunPython(update_all_pages_with_ctas)
    ]