from django.test import TestCase, override_settings
from wagtail.models import Page, Site

from wdi_website.models.models import Signature
from wdi_website.models.pages import CountryListPage, SignatureListPage


def make_one_country_list_page(title="Test", slug="test", hostname="example.com"):
    root_page = Page(title="Root", depth=1, path="/")
    root_page.save()

    test_site = Site.objects.get()
    test_site.root_page = root_page
    test_site.save()

    page = CountryListPage(title=title, slug=slug)
    root_page.add_child(instance=page)
    page.set_url_path(parent=root_page)
    page.save()


def wipe_all_signatures():
    all_signatures = Signature.objects.all()
    for s in all_signatures:
        s.delete()


five_signatures_all_confirmed_same_country = {
    "signatures": [
        Signature(
            name="Test",
            country="US",
            email="test123@example.com",
            subscribe_to_emails=False,
            sign_for_organization=False,
            language="en",
            email_confirmed=True,
        ),
        Signature(
            name="Jane Doe",
            country="US",
            email="aaa@bbb.ccc",
            subscribe_to_emails=True,
            sign_for_organization=True,
            language="es",
            email_confirmed=True,
        ),
        Signature(
            name="A" * 255,
            country="US",
            email="a255@mail.ru",
            subscribe_to_emails=True,
            sign_for_organization=False,
            language="ru",
            email_confirmed=True,
        ),
        Signature(
            name="10" * 100,
            country="US",
            email="test1009000999@534545.com",
            subscribe_to_emails=False,
            sign_for_organization=True,
            language="en",
            email_confirmed=True,
        ),
        Signature(
            name="Test Test Test YYYYY",
            country="US",
            email="456TeSt@Test.test",
            subscribe_to_emails=False,
            sign_for_organization=False,
            language="de",
            email_confirmed=True,
        ),
    ],
    "names": ["Test", "Jane Doe", "A" * 255, "10" * 10, "Test Test Test YYYYY"],
    "countries": ["United States of America"],
}
five_signatures_two_unconfirmed = {
    "signatures": [
        Signature(
            name="Test",
            country="US",
            email="tes0t123@example.com",
            subscribe_to_emails=False,
            sign_for_organization=False,
            language="en",
            email_confirmed=True,
        ),
        Signature(
            name="Jane Doe",
            country="GB",
            email="aa0a@bbb.ccc",
            subscribe_to_emails=True,
            sign_for_organization=True,
            language="es",
            email_confirmed=True,
        ),
        Signature(
            name="A" * 255,
            country="NL",
            email="a2505@mail.ru",
            subscribe_to_emails=True,
            sign_for_organization=False,
            language="ru",
            email_confirmed=False,
        ),
        Signature(
            name="10" * 100,
            country="ES",
            email="test01009000999@534545.com",
            subscribe_to_emails=False,
            sign_for_organization=True,
            language="en",
            email_confirmed=True,
        ),
        Signature(
            name="Test Test Test YYYYY",
            country="RU",
            email="456T0eSt@Test.test",
            subscribe_to_emails=False,
            sign_for_organization=False,
            language="de",
            email_confirmed=False,
        ),
    ],
    "names": ["Test", "Jane Doe", "10" * 10],
    "countries": ["United States of America", "United Kingdom", "Spain"],
}
five_signatures_all_unconfirmed_none_displayed = {
    "signatures": [
        Signature(
            name="Test",
            country="US",
            email="test123@examp1le.com",
            subscribe_to_emails=False,
            sign_for_organization=False,
            language="en",
            email_confirmed=False,
        ),
        Signature(
            name="Jane Doe",
            country="GB",
            email="aaa@bb1b.ccc",
            subscribe_to_emails=True,
            sign_for_organization=True,
            language="es",
            email_confirmed=False,
        ),
        Signature(
            name="A" * 255,
            country="NL",
            email="a255@m1ail.ru",
            subscribe_to_emails=True,
            sign_for_organization=False,
            language="ru",
            email_confirmed=False,
        ),
        Signature(
            name="10" * 100,
            country="ES",
            email="test10090009199@534545.com",
            subscribe_to_emails=False,
            sign_for_organization=True,
            language="en",
            email_confirmed=False,
        ),
        Signature(
            name="Test Test Test YYYYY",
            country="RU",
            email="456TeSt@T1est.test",
            subscribe_to_emails=False,
            sign_for_organization=False,
            language="de",
            email_confirmed=False,
        ),
    ],
    "names": [],
    "countries": [],
}


@override_settings(
    STATICFILES_STORAGE="django.contrib.staticfiles.storage.StaticFilesStorage"
)
class TestCountryListPage(TestCase):
    def test_country_names_displayed_for_confirmed_signatures_only(self):
        test_cases = [
            five_signatures_all_confirmed_same_country,
            five_signatures_all_unconfirmed_none_displayed,
            five_signatures_two_unconfirmed,
        ]

        make_one_country_list_page(slug="countries")

        for test_case in test_cases:
            signatures_list = test_case["signatures"]
            countries_list = test_case["countries"]
            with self.subTest(name=",".join(countries_list)):
                wipe_all_signatures()  # Remove signatures from past subtests
                for signature in signatures_list:
                    signature.save()

                resp = self.client.get("/countries/", follow=True)
                assert resp.status_code == 200
                for country in countries_list:
                    self.assertContains(resp, country)


def make_one_signature_list_page(
    title="Test",
    slug="test",
):
    root_page = Page(title="Root", depth=1, path="/")
    root_page.save()

    test_site = Site.objects.get()
    test_site.root_page = root_page
    test_site.save()

    page = SignatureListPage(title=title, slug=slug)
    root_page.add_child(instance=page)
    page.set_url_path(parent=root_page)
    page.save()


@override_settings(
    STATICFILES_STORAGE="django.contrib.staticfiles.storage.StaticFilesStorage"
)
class TestSignatureListPage(TestCase):
    def test_names_displayed_for_confirmed_signatures_only(self):
        test_cases = [
            five_signatures_all_confirmed_same_country,
            five_signatures_all_unconfirmed_none_displayed,
            five_signatures_two_unconfirmed,
        ]

        make_one_signature_list_page(slug="signatures")

        for test_case in test_cases:
            signatures_list = test_case["signatures"]
            names_list = test_case["countries"]
            with self.subTest(name=",".join(names_list)):
                wipe_all_signatures()  # Remove signatures from past subtests
                for signature in signatures_list:
                    signature.save()

                resp = self.client.get("/signatures/", follow=True)
                assert resp.status_code == 200
                for name in names_list:
                    self.assertContains(resp, name)

    def test_countries_displayed_for_confirmed_signatures_only(self):
        test_cases = [
            five_signatures_all_confirmed_same_country,
            five_signatures_all_unconfirmed_none_displayed,
            five_signatures_two_unconfirmed,
        ]

        make_one_signature_list_page(slug="signatures")

        for test_case in test_cases:
            signatures_list = test_case["signatures"]
            countries_list = test_case["countries"]
            with self.subTest(name=",".join(countries_list)):
                wipe_all_signatures()  # Remove signatures from past subtests
                for signature in signatures_list:
                    signature.save()

                resp = self.client.get("/signatures/", follow=True)
                assert resp.status_code == 200
                for country in countries_list:
                    self.assertContains(resp, country)
