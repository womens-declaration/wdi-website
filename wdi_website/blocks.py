from wagtail import blocks
from wagtail.images.blocks import ImageChooserBlock
from django.utils.translation import gettext_lazy as _

from dal_select2.widgets import ModelSelect2

from .models.models import Organization, Signature
from .forms import SignatureWithOrganizationForm

class LinkBlock(blocks.StructBlock):
    """
    Allows a user to optionally link the containing block to a Page or absolute URL.
    """
    page = blocks.PageChooserBlock(
        required=False,
        help_text="Link to the chosen page."
    )
    external_url = blocks.URLBlock(
        required=False,
        help_text="Link to the given URL. If filled in, will overwrite the Page (the field above)"
    )

    def get_url(self, value):
        if value["external_url"] and len(value["external_url"]) > 0:
            return value["external_url"]
        elif value["page"]:
            return value["page"].get_url_parts()[2]
        else:
            return ""

    def render_basic(self, value, context=None):
        return self.get_url(value)

    class Meta:
        label = 'Link'
        form_classname = 'link-block'

class SignDeclarationBlock(blocks.StaticBlock):
    def get_context(self, value, parent_context=None):


        context = super().get_context(value, parent_context)

        if "sign_declaration_form_data" in context["request"].session:
            form_data = context["request"].session["sign_declaration_form_data"]
            context["form"] = SignatureWithOrganizationForm(form_data)
        else:
            context["form"] = SignatureWithOrganizationForm()

        return context

    class Meta:
        admin_text = _("Sign the Declaration form: no config")
        template = "blocks/sign_dec_form.html"


class DeclarationStatsBlock(blocks.StaticBlock):
    def get_context(self, value, parent_context=None):

        context = super().get_context(value, parent_context)
        context["signature_count"] = Signature.objects.filter(
            email_confirmed=True
        ).count()
        context["organization_count"] = Organization.objects.filter(
            confirmed=True
        ).count()
        context["country_count"] = (
            Signature.objects.filter(email_confirmed=True)
                .values("country")
                .distinct()
                .count()
        )
        return context

    class Meta:
        admin_text = _("Stats about the Declaration: no config")
        template = "blocks/declaration_stats.html"


class OrganizationsListBlock(blocks.StaticBlock):
    def get_context(self, value, parent_context=None):

        context = super().get_context(value, parent_context)
        context["organizations_activist"] = Organization.objects.filter(
            confirmed=True, type=Organization.ACTIVIST
        )
        context["organizations_nonprofit"] = Organization.objects.filter(
            confirmed=True, type=Organization.NONPROFIT
        )
        context["organizations_business"] = Organization.objects.filter(
            confirmed=True, type=Organization.BUSINESS
        )
        context["organizations_web"] = Organization.objects.filter(
            confirmed=True, type=Organization.WEB
        )
        context["organizations_other"] = Organization.objects.filter(
            confirmed=True, type=Organization.OTHER
        )

        return context

    class Meta:
        admin_text = (_("Supporting Orgs List: no config"),)
        template = ("blocks/org_list.html",)
        icon = "group"


def get_confirmed_orgs():
    return [(org.id, org.name) for org in Organization.objects.filter(confirmed=True).order_by('name')]


class FeaturedOrganizationsListBlock(blocks.StructBlock):
    featured_organizations = blocks.ListBlock(
        blocks.ChoiceBlock(choices=get_confirmed_orgs, icon="group", label="Select a Featured Organization here",
                           widget=ModelSelect2)
    )
    also_add_3_latest = blocks.BooleanBlock(help_text="Include 3 latest organizations in addition to selected ones",
                                            required=False)

    def get_context(self, value, parent_context=None):
        context = super(FeaturedOrganizationsListBlock, self).get_context(value, parent_context)
        found_organizations = list(Organization.objects.filter(id__in=value["featured_organizations"]))

        # Rearrange the list to preserve the order of orgs selected by user.
        context_list = []
        for i in value["featured_organizations"]:
            for o in found_organizations:
                if o.id == int(i):
                    context_list.append(o)
                    break

        if value["also_add_3_latest"] is True:  # If we add 3 latest organizations, query them and add to resulting list
            latest_organizations = list(Organization.objects.filter(confirmed=True).order_by('-id')[:3])
            context_list.extend(latest_organizations)

        context["featured_organizations"] = context_list
        return context

    class Meta:
        admin_text = "Select several featured organizations"
        template = "blocks/featured_organizations.html"
        help_text = "Select featured organisations with a dropdown box. Add new dropdowns with a plus sign."
        icon = "pick"


class FeaturedSignatoriesBlock(blocks.StructBlock):
    """Featured Signatories with optional image, description, and quote."""
    featured_signatories = blocks.ListBlock(
        blocks.StructBlock(
            [
                ("image", ImageChooserBlock(required=False)),
                ("signatory_name", blocks.CharBlock(required=True, max_length=100, help_text="Character limit: 100")),
                ("signatory_descriptor", blocks.TextBlock(required=False, max_length=60, help_text="Character limit: 60")),
                ("signatory_quote", blocks.TextBlock(required=False, max_length=200, help_text="Character limit: 200")),
                ("signatory_quote_source", blocks.URLBlock(required=False)),
            ],
            label='Enter the details of a featured signatory here',
            icon="user"
        )
    )

    class Meta:
        admin_text = "Showcase signatories with the option of including their image, description, and a quote"
        help_text = "Showcase signatories with the option of including their image, description, and a quote"
        template = "blocks/featured_signatories_list_block.html"
        icon = "pick"
        label = "Featured Signatories"


class CtaBlock(blocks.StructBlock):
    button_text = blocks.CharBlock()
    button_link = LinkBlock()

    class Meta:
        icon = "redirect"
        template = "blocks/cta_std.html"


class BodyBlock(blocks.StreamBlock):
    page_divider = blocks.StaticBlock(
        admin_text=_("Female symbol page divider: no config"),
        template="blocks/female_divider.html",
        icon="horizontalrule",
    )
    declaration_stats = DeclarationStatsBlock(icon="list-ol")
    sign_the_declaration = SignDeclarationBlock(icon="edit")
    basic_paragraph = blocks.StructBlock(
        [
            ("title", blocks.CharBlock()),
            ("body", blocks.RichTextBlock()),
        ],
        help_text=_(
            'A simple paragraph with a title and rich text. For youtube.com embeds, ending the url with "&rel=0" will ensure only videos from the same channel will be advertised at the end.'
        ),
        template="blocks/basic_paragraph.html",
        icon="pilcrow",
    )
    paragraph_with_image = blocks.StructBlock(
        [
            ("title", blocks.CharBlock(required=False)),
            ("body", blocks.RichTextBlock()),
            ("image", ImageChooserBlock()),
        ],
        help_text=_("A paragraph with a title, rich text, and image"),
        template="blocks/paragraph_with_image.html",
        icon="image",
    )
    cta_block = CtaBlock()

    country_contact_form = blocks.StaticBlock(
        admin_text=_("Country Contact Form: no config"),
        template="blocks/country_contact_form.html",
        icon="form",
    )
    volunteer_form = blocks.StaticBlock(
        admin_text=_("Volunteer Form: no config"),
        template="blocks/volunteer_form.html",
        icon="form",
    )
    donate_widget = blocks.StaticBlock(
        admin_text=_("Donate Widget: no config"),
        template="blocks/donate_widget.html",
        icon="pick",
    )
    org_list = OrganizationsListBlock(icon="group")
    social_media_block = blocks.StructBlock(
        [
            ("spinster_link", blocks.URLBlock(required=False)),
            ("twitter_link", blocks.URLBlock(required=False)),
            ("instagram_link", blocks.URLBlock(required=False)),
            ("telegram_link", blocks.URLBlock(required=False)),
            ("faceBook_link", blocks.URLBlock(required=False)),
            ("youTube_link", blocks.URLBlock(required=False)),
            ("patreon_link", blocks.URLBlock(required=False)),
            ("goFundMe_link", blocks.URLBlock(required=False)),
            ("crowdJustice_link", blocks.URLBlock(required=False)),
            ("website_link", blocks.URLBlock(required=False)),
        ],
        help_text=_("Social media icons with external links."),
        template="blocks/social_media_block.html",
        icon="site",
    )
    embed_resource_block = blocks.StructBlock(
        [
            ("height", blocks.IntegerBlock(default="520")),
            ("embedded_resource_link", blocks.URLBlock()),
            ("direct_resource_link", blocks.URLBlock()),
            ("button_name", blocks.CharBlock()),
            ("source_of_resource", blocks.CharBlock()),
        ],
        help_text=_(
            'Embed an external resource or website. For an embedded google form link, get the url from the src="" portion of the embed option under "Send" in google forms. The button name (e.g. "XX form" or "Watch Video") and source of resource (e.g. "(via Google)" or "(via Youtube)") are for the no-cookie option.'
        ),
        template="blocks/embed_resource_block.html",
        icon="form",
    )
    featured_organization_block = FeaturedOrganizationsListBlock()
    featured_signatories_block = FeaturedSignatoriesBlock()
