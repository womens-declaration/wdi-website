from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '_a&i2oax)4gq)o+u@bfknqxguht_$b(04)k=e(k!gc3v*%i^!$'

# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = ['*']

WAGTAILADMIN_BASE_URL = 'http://localhost:8000'

ADMIN_NAME = 'admin'

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
#EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# GA_VIEW_ID = "000000-000000"

SILENCED_SYSTEM_CHECKS = ['captcha.recaptcha_test_key_error']

# email settings
# https://docs.djangoproject.com/en/3.0/ref/settings/#email-host
# You can use a development SMTP server to test sending e-mails. Example: http://nilhcem.com/FakeSMTP/
EMAIL_HOST ='localhost'
EMAIL_PORT = 2525

TO_ADDRESS_ORG_REGISTERED = 'info@womensdeclaration.com'

#EMAIL_HOST_USER = 'wdi'
#EMAIL_HOST_PASSWORD = 'wdi'

WAGTAIL_SITE_NAME = "Women's Declaration International"
WAGTAIL_2FA_REQUIRED = False

CSP_STYLE_SRC = ("'self'", "'unsafe-inline'",)
CSP_SCRIPT_SRC = ("'self'", "'unsafe-inline'",)
CSP_FONT_SRC = ("'self'",)
CSP_IMG_SRC = ("'self'", "i.ytimg.com")
CSP_FRAME_SRC = ("'self'",
                 "docs.google.com",
                 "www.youtube-nocookie.com",
                 )
CSP_FRAME_ANCESTORS = ("'self'")

try:
    from .local import *
except ImportError:
    pass
