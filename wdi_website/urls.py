from django.conf import settings
from django.conf.urls import include
from django.urls import path, re_path
from django.conf.urls.i18n import i18n_patterns

from wagtail.admin import urls as wagtailadmin_urls
from wagtail import urls as wagtail_urls
from wagtail.documents import urls as wagtaildocs_urls

from search import views as search_views

from wdi_website.views import submit_signature as wdi_signature

urlpatterns = [
    path(settings.ADMIN_NAME + r'/', include(wagtailadmin_urls)),
    path('documents/', include(wagtaildocs_urls)),

    path('submit/signature/', wdi_signature.submit_signature),
    re_path('verify/(?P<input_token>[0-9a-fA-F]{32})$', wdi_signature.verify_signature),
    path('captcha/', include('captcha.urls')),
    path('chaining/', include('smart_selects.urls')),
    path('i18n/', include('django.conf.urls.i18n')),
]

urlpatterns += i18n_patterns(
    path('search/', search_views.search, name='search'),

    # For anything not caught by a more specific rule above, hand over to
    # Wagtail's page serving mechanism. This should be the last pattern in
    # the list:
    path(r'', include(wagtail_urls)),
)

handler404 = 'wdi_website.views.errors.view_404'
handler500 = 'wdi_website.views.errors.view_500'

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
