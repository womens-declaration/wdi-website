from django.urls import re_path
from django.utils.translation import gettext_lazy as _
from wagtail.admin.panels import (
    FieldPanel,
    extract_panel_definitions_from_model_class,
)

from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register
from wagtail.contrib.modeladmin.helpers import ButtonHelper
from wagtail import hooks

from wdi_website.utils.image_input_widget import ImageInput
from wdi_website.models.models import Signature, Organization, NewsItem
from wdi_website.views import csv_export as wdi_csv
from wdi_website.views import submit_signature as wdi_signature


class ResendConfirmationEmailButtonHelper(ButtonHelper):
    # Define classes for our button, here we can set an icon for example
    view_button_classnames = ["button-small", "button-secondary"]

    def view_button(self, obj):
        # Define a label for our button
        text = "Resend confirmation email"
        return {
            "url": "resend/%3Femail={}".format(
                obj.email
            ),  # decide where the button links to
            "label": text,
            "classname": self.finalise_classname(self.view_button_classnames),
            "title": text,
        }

    def get_buttons_for_obj(
        self, obj, exclude=None, classnames_add=None, classnames_exclude=None
    ):
        """
        This function is used to gather all available buttons.
        We append our custom button to the btns list.
        """
        btns = super().get_buttons_for_obj(
            obj, exclude, classnames_add, classnames_exclude
        )
        if "view" not in (exclude or []) and not obj.email_confirmed:
            btns.append(self.view_button(obj))
        return btns


class SignatureAdmin(ModelAdmin):
    model = Signature
    menu_label = _("Signatures")  # ditch this to use verbose_name_plural from model
    menu_icon = "user"  # change as required
    menu_order = 200  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    exclude_from_explorer = (
        False  # or True to exclude pages of this type from Wagtail's explorer view
    )
    list_display = ("name", "country", "organization", "timestamp", "email_confirmed")
    list_filter = (
        "email_confirmed",
        "subscribe_to_emails",
        "country",
        "organization",
        "sign_for_organization",
    )
    search_fields = ("email", "name", "organization")
    button_helper_class = ResendConfirmationEmailButtonHelper


class OrganizationAdmin(ModelAdmin):
    model = Organization
    menu_label = _("Organizations")  # ditch this to use verbose_name_plural from model
    menu_icon = "group"  # change as required
    menu_order = 300  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    exclude_from_explorer = (
        False  # or True to exclude pages of this type from Wagtail's explorer view
    )
    list_display = ("name", "country", "type", "confirmed")
    list_filter = (
        "permission_logo",
        "permission_support",
        "international",
        "type",
        "country",
    )
    search_fields = ("name",)

    # Auto-generate all fields except for the Logo.
    panels = extract_panel_definitions_from_model_class(
        Organization, exclude=["logo"]
    ) + [
        FieldPanel("logo", widget=ImageInput)
    ]  # The Logo field uses a custom widget.


class NewsItemAdmin(ModelAdmin):
    model = NewsItem
    menu_label = _("News Item")  # ditch this to use verbose_name_plural from model
    menu_icon = "title"  # change as required
    menu_order = 300  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    exclude_from_explorer = (
        False  # or True to exclude pages of this type from Wagtail's explorer view
    )
    list_display = ("headline", "language", "timestamp")
    list_filter = ("language",)
    search_fields = ("headline", "preview_text", "cta_title" "cta_subtitle")


# Now you just need to register your customised ModelAdmin class with Wagtail
modeladmin_register(SignatureAdmin)
modeladmin_register(OrganizationAdmin)
modeladmin_register(NewsItemAdmin)


# Signature resend confirmation email
@hooks.register("register_admin_urls")
def urlconf_resend_confirmation_email():
    return [
        re_path(
            "wdi_website/signature/resend/\?email=(?P<email>[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+)",
            wdi_signature.resend_confirmation_email,
            name="signature_resend_confirmation_email",
        )
    ]


# Signature CSV export URL
@hooks.register("register_admin_urls")
def urlconf_signatures_export_csv():
    return [
        re_path(
            "wdi_website/signature/export/\?country=(?P<country>[A-Z]{0,2})&subscribed=(?P<subscribed>[0-1]{0,1})&confirmed=(?P<confirmed>[0-1]{0,1})",
            wdi_csv.signatures_export_csv,
            name="signatures_export_csv",
        )
    ]


# Organization CSV export URL
@hooks.register("register_admin_urls")
def urlconf_organizations_export_csv():
    return [
        re_path(
            "wdi_website/organization/export/"
            "\?country=(?P<country>[A-Z]{0,2})"
            "&international=(?P<international>[0-1]{0,1})"
            "&permission_support=(?P<permission_support>[0-1]{0,1})"
            "&permission_logo=(?P<permission_logo>[0-1]{0,1})"
            "&confirmed=(?P<confirmed>[0-1]{0,1})"
            "&type=(?P<type>[A-Z]{0,9})",
            wdi_csv.organizations_export_csv,
            name="organizations_export_csv",
        )
    ]


@hooks.register("construct_settings_menu")
def hide_workflows(request, menu_items):
    # Keep all the menu items, except those that start with "Workflow"
    # As of July 2023, that will hide "Workflows" and "Workflow tasks"
    menu_items[:] = [
        item for item in menu_items if not item.name.lower().startswith("workflow")
    ]
