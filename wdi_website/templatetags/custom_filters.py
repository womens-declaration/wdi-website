from unidecode import unidecode
from django.template.defaultfilters import slugify
from django import template

register = template.Library()

def slug(value):
    return slugify(unidecode(value))
        
register.filter('slug', slug)