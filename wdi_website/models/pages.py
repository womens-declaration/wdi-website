from django.core.paginator import Paginator
from django.db import models
from django.utils.translation import gettext_lazy as _
from django_countries import countries
from wagtail.admin.panels import FieldPanel
from wagtail.fields import StreamField
from wagtail.models import Page
from wagtail.search import index

from wdi_website.blocks import BodyBlock, CtaBlock
from wdi_website.models.models import Signature, NewsItem, Organization


class StandardPage(Page):
    subtitle = models.CharField(
        max_length=255,
        help_text=_("A subtitle to show on the header of the page"),
        blank=True,
        verbose_name=_("Subtitle"),
    )

    header_ctas = StreamField([('cta', CtaBlock())], blank=True, verbose_name=_("Header CTAs"), use_json_field=True)
    body = StreamField(BodyBlock(required=False), blank=True, verbose_name=_("Body"), use_json_field=True)

    content_panels = Page.content_panels + [
        FieldPanel("subtitle"),
        FieldPanel("header_ctas"),
        FieldPanel("body"),
    ]

    search_fields = Page.search_fields + [
        index.SearchField("body"),
    ]


class CountryListPage(Page):
    subtitle = models.CharField(
        max_length=255,
        help_text=_("A subtitle to show on the header of the page"),
        blank=True,
        verbose_name=_("Subtitle"),
    )

    header_ctas = StreamField([('cta', CtaBlock())], blank=True, verbose_name=_("Header CTAs"), use_json_field=True)

    content_panels = Page.content_panels + [
        FieldPanel("subtitle"),
        FieldPanel("header_ctas"),
    ]

    # TODO: cache
    def get_context(self, request):
        context = super().get_context(request)

        signatures_per_country = {}
        country_codes_to_countries = {}
        for c in countries:
            country_codes_to_countries[c.code] = c
            signatures_per_country[c.code] = 0

        signatures = Signature.objects.filter(email_confirmed=True)
        signature_countries = set()

        for signature in signatures:
            signatures_per_country[signature.country] = (
                signatures_per_country[signature.country] + 1
            )
            signature_countries.add(signature.country)

        context["country_codes_to_countries"] = country_codes_to_countries
        context["countries"] = sorted(signature_countries, key=lambda i: i.name)
        context["signatures_per_country"] = signatures_per_country
        context["country_count"] = len(signature_countries)

        return context


class SignatureListPage(Page):
    subtitle = models.CharField(
        max_length=255,
        help_text=_("A subtitle to show on the header of the page"),
        blank=True,
        verbose_name=_("Subtitle"),
    )

    header_ctas = StreamField([('cta', CtaBlock())], blank=True, verbose_name=_("Header CTAs"), use_json_field=True)

    content_panels = Page.content_panels + [
        FieldPanel("subtitle"),
        FieldPanel("header_ctas"),
    ]

    def get_context(self, request):
        context = super().get_context(request)

        signatures_list = Signature.objects.filter(email_confirmed=True).order_by(
            "-timestamp"
        )
        # TODO: Remove this
        organizations_list = Organization.objects.all().order_by("name")
        paginator = Paginator(signatures_list, 200)

        page = request.GET.get("page")
        signatures = paginator.get_page(page)

        context["signatures"] = signatures
        context["organizations"] = organizations_list
        return context


class NewsItemListPage(Page):
    subtitle = models.CharField(
        max_length=255,
        help_text=_("A subtitle to show on the header of the page"),
        blank=True,
        verbose_name=_("Subtitles"),
    )

    header_ctas = StreamField([('cta', CtaBlock())], blank=True, verbose_name=_("Header CTAs"), use_json_field=True)

    content_panels = Page.content_panels + [
        FieldPanel("subtitle"),
        FieldPanel("header_ctas"),
    ]

    def get_context(self, request):
        context = super().get_context(request)

        new_items_list = NewsItem.objects.all().order_by("-timestamp")
        paginator = Paginator(new_items_list, 9)

        page = request.GET.get("page")
        news_items = paginator.get_page(page)

        context["news_items"] = news_items
        return context

