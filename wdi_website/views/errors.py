from django.shortcuts import render

from django.utils.translation import gettext as _


def view_404(request, exception=None):
    error_context = {
        "error_code": 404,
        "error_title": "Page not found",
        "error_description": _("Sorry, this page does not exist."),
    }
    return render(request, "error.html", status=404, context=error_context)


def view_500(request):
    error_context = {
        "error_code": 500,
        "error_title": "Internal server error",
        "error_description": _(
            "Sorry. Something went wrong on our side. Try again later."
        ),
    }
    return render(request, "error.html", status=500, context=error_context)


def view_403(request):
    error_context = {
        "error_code": 403,
        "error_title": "Forbidden",
        "error_description": _(
            "Sorry. You do not have a permission to access this resource."
        ),
    }
    return render(request, "error.html", status=403, context=error_context)


def view_400(request):
    error_context = {
        "error_code": 400,
        "error_title": "Bad Request",
        "error_description": _(
            "Sorry. There is something wrong with data you have sent. Check your input and try "
            "again."
        ),
    }
    return render(request, "error.html", status=400, context=error_context)
