import csv, re
from django.core.management.base import BaseCommand
from django_countries import countries
from dateutil.parser import parse as parse_date

from wdi_website.models.models import Signature


class Command(BaseCommand):
    help = 'Imports signatures from a .csv file'

    def add_arguments(self, parser):
        parser.add_argument('filepath', nargs=1, type=str)

    def handle(self, *args, **options):
        with open(options['filepath'][0]) as f:
            reader = csv.reader(f)
            for row in reader:
                self.handle_row(row)

        self.stdout.write(self.style.SUCCESS(
            'Successfully imported signatures.'))

    def handle_row(self, row, *args, **options):
        country = parse_country(row[2])
        if not country:
            self.stdout.write(self.style.WARNING('{} | {} | {} | {}  | SKIPPED due to invalid country.'.format(row[0], row[1], row[2], row[3])))
            return  # skip invalid countries

        # FIXME: Skip existing entries
        signature = Signature.objects.create(
            name=row[1],
            country=country,
            email=row[3],
            subscribe_to_emails=(row[4] == "TRUE"),
            organization=row[5],
            sign_for_organization=(row[6] == "TRUE")
        )
        signature.timestamp = parse_date(row[0])
        signature.save()
        self.stdout.write(self.style.SUCCESS(
            '{} from {} signed the declaration at {}.'.format(
                signature.name,
                signature.country,
                signature.timestamp,
            )))


def parse_country(country_string):
    countries_list = list(countries)

    fixed_code = re.sub('[., ]', '', country_string).upper()
    fixed_name = country_string.strip()

    for i, v in enumerate(countries_list):
        code, name = v
        if fixed_code == code or fixed_name == name:
            return code

    return None
